from typing import Iterator

import pytest
from pytest import fixture
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service

from app.web_app import WebApp

service: Service = Service()


@fixture(scope='session')
def driver():
    options = Options()
    options.headless = False
    driver = webdriver.Chrome(service=service)
    driver.maximize_window()
    yield driver
    driver.quit()


@pytest.fixture
def veeam_site_opened(driver) -> Iterator[WebApp]:
    app = WebApp(driver)
    app.open()
    yield app