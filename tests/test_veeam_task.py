from selenium.webdriver.common.by import By
from retry import retry
from app.pages.vacancies_page import VacanciesPage
from waitable_element import waitable_element

JOBS_TOTAL: int = 3


def test_veeam_task(veeam_site_opened, driver):
    page = VacanciesPage

    waitable_element(page.all_departments_btn, driver).click()
    waitable_element(page.research_and_development, driver).click()

    waitable_element(page.all_languages, driver).click()
    waitable_element(page.english_language, driver).click()

    # There were 3 jobs available with this configuration during test task performance(!!!!)
    assert_total_jobs_in_list(driver, expected_value=JOBS_TOTAL)


@retry(exceptions=AssertionError, tries=3, delay=0.5)
def assert_total_jobs_in_list(driver, expected_value: int):
    total_jobs_in_the_list: int = len(driver.find_elements(By.CSS_SELECTOR, ".card.card-sm.card-no-hover"))
    assert total_jobs_in_the_list == expected_value
