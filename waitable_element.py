from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def waitable_element(xpath_locator: str, driver):

    return WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.XPATH, xpath_locator))
    )
