class VacanciesPage:

    all_departments_btn: str = "//button[contains(text(),'All departments')]"
    research_and_development: str = "//a[contains(text(),'Research & Development')]"
    all_languages: str = "//button[contains(text(),'All languages')]"
    english_language: str = "//label[contains(text(),'English')]"
